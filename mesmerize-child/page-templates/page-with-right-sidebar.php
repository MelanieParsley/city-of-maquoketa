<?php
/*
Template Name: Page With Right Sidebar
*/

mesmerize_get_header();
?>
    <div class="page-content">
		
		<?php if ( !is_front_page() ) { ?>
		<div class="header-wrapper">
			<div <?php echo mesmerize_header_background_atts(); ?>>
				<?php do_action( 'mesmerize_before_header_background' ); ?>
				<?php mesmerize_print_video_container(); ?>
						<?php mesmerize_print_inner_pages_header_content(); ?>
				<?php mesmerize_print_header_separator(); ?>
			</div>
		</div>
		<?php } ?>
		
        <div class="gridContainer">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-9">
                    <?php
                    while (have_posts()) : the_post();
                        the_content();
                    endwhile;
                    ?>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-3 page-sidebar-column">
                    <?php mesmerize_get_sidebar('pages'); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
