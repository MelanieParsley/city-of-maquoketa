	<?php //mesmerize_get_footer_content(); ?>
	<div class="footer footer-simple">
    <div class="footer-content center-xs">
        <div class="gridContainer">
	        <div class="row middle-xs footer-content-row">
	            <div class="footer-content-col col-xs-12">
					<div id="footer-left-content" class="copyright"><p>City of Maquoketa &bull; 201 East Pleasant Street, Maquoketa, IA 52060 &bull; (563) 652-2484<br />Copyright &copy; 2020 <a href="https://maquoketaia.com">City of Maquoketa</a>. All Rights Reserved.</p></div>
					<div id="footer-right-content" class="powered"><p>Site Designed and Powered By <a target="_blank" href="https://net-smart.net">Net-Smart</a>.</p></div>
					<div id="employees-only"><p><a href="https://maquoketaia.com/information-for-employees/">City Employees</a></p></div>
				</div>
	        </div>
	    </div>
    </div>
</div>
	</div>
<?php wp_footer(); ?>
</body>
</html>
