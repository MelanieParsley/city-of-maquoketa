<?php mesmerize_get_header(); ?>

    <div class="page-content">	
	
		<div class="header-wrapper">
			<div <?php echo mesmerize_header_background_atts(); ?>>
				<?php do_action( 'mesmerize_before_header_background' ); ?>
				<?php mesmerize_print_video_container(); ?>
						<?php mesmerize_print_inner_pages_header_content(); ?>
				<?php mesmerize_print_header_separator(); ?>
			</div>
		</div>
	
        <div class="<?php mesmerize_page_content_wrapper_class(); ?>">
            <?php
            while (have_posts()) : the_post();
                get_template_part('template-parts/content', 'page');
            endwhile;
            ?>
        </div>
    </div>

<?php get_footer(); ?>
