<?php
 
function dailycallreports_function() {
	ob_start(); ?>
	<div class="pdfList">
		<ul>
		<?php 
		$dirname = "wp-content/uploads/DailyCallReports/";
		$images = glob($dirname."*.*");
		usort($images, create_function('$b,$a', 'return filemtime($a) - filemtime($b);'));
		foreach($images as $image) {
			$path_parts = pathinfo($image);
			echo '<li><a href="'.home_url().'/wp-content/uploads/DailyCallReports/'.$path_parts['filename'].'.pdf" target="_blank">'.$path_parts['filename'].'</a></li>';
		}
			?>
		</ul>
	</div>
	<?php
	$ReturnString = ob_get_contents();
	ob_end_clean();
	return $ReturnString;
}
add_shortcode('dailycallreports', 'dailycallreports_function');
 
function ordinances_function() {
	ob_start(); ?>
	<div class="pdfList">
		<ul>
		<?php 
		$dirname = "wp-content/uploads/Ordinances/";
		$images = glob($dirname."*.*");
		
		foreach($images as $image) {
			$path_parts = pathinfo($image);
			echo '<li><a href="'.home_url().'/wp-content/uploads/Ordinances/'.$path_parts['filename'].'.pdf" target="_blank">'.$path_parts['filename'].'</a></li>';
		}
			?>
		</ul>
	</div>
	<?php
	$ReturnString = ob_get_contents();
	ob_end_clean();
	return $ReturnString;
}
add_shortcode('ordinances', 'ordinances_function');

function agendas_function() { 
	ob_start(); ?>
	<div class="pdfList">
		<ul>
		<?php 
		$dirname = "wp-content/uploads/Agendas/";
		$images = glob($dirname."*.*");
		usort($images, create_function('$b,$a', 'return filemtime($a) - filemtime($b);'));
		foreach($images as $image) {
			$path_parts = pathinfo($image);
			echo '<li><a href="'.home_url().'/wp-content/uploads/Agendas/'.$path_parts['filename'].'.pdf" target="_blank">'.$path_parts['filename'].'</a></li>';
		}
			?>
		</ul>
	</div>
	<?php
	$ReturnString = ob_get_contents();
	ob_end_clean();
	return $ReturnString;
}
add_shortcode('agendas', 'agendas_function');

function minutes_function() { 
	ob_start(); ?>
	<div class="pdfList">
		<ul>
		<?php 
		$dirname = "wp-content/uploads/Minutes/";
		$images = glob($dirname."*.*");
		usort($images, create_function('$b,$a', 'return filemtime($a) - filemtime($b);'));
		foreach($images as $image) {
			$path_parts = pathinfo($image);
			echo '<li><a href="'.home_url().'/wp-content/uploads/Minutes/'.$path_parts['filename'].'.pdf" target="_blank">'.$path_parts['filename'].'</a></li>';
		}
			?>
		</ul>
	</div>
	<?php
	$ReturnString = ob_get_contents();
	ob_end_clean();
	return $ReturnString;
}
add_shortcode('minutes', 'minutes_function');

function compPlan_function() { 
	ob_start(); ?>
	<div class="pdfList">
		<ul>
		<?php 
			$dirname = "wp-content/uploads/2040CompPlan/";
			$images = glob($dirname."*.*");

			foreach($images as $image) {
				$path_parts = pathinfo($image);
				echo '<li><a href="'.home_url().'/wp-content/uploads/2040CompPlan/'.$path_parts['filename'].'.pdf" target="_blank">'.$path_parts['filename'].'</a></li>';
			}
			?>
		</ul>
	</div>
	<?php
	$ReturnString = ob_get_contents();
	ob_end_clean();
	return $ReturnString;
}
add_shortcode('compPlan', 'compPlan_function');
?>