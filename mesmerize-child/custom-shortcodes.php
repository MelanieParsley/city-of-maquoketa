<?php

function getfilesfromfolder($dirname, $sort) {
	ob_start(); ?>
	<div class="pdfList">
		<ul>
		<?php  
		$images = glob($dirname."*.*");
		if ($sort == "alpha") {
			sort($images); //Sort files alphabetically
		} elseif ($sort == "date") {
			usort($images, create_function('$b,$a', 'return filemtime($a) - filemtime($b);')); //Sort files by date
		}
		foreach($images as $image) {
			$path_parts = pathinfo($image);
			//echo '<li><a href="'.home_url().'/'.$dirname.''.$path_parts['filename'].'.pdf" target="_blank">'.$path_parts['filename'].'</a></li>';
			echo '<li><a href="'.home_url().'/'.$dirname.''.$path_parts['filename'].'.'.$path_parts['extension'].'" target="_blank">'.$path_parts['filename'].'</a></li>';
		}
			?>
		</ul>
	</div>
	<?php
	$ReturnString = ob_get_contents();
	ob_end_clean();
	return $ReturnString;
}
 
function dailycallreports_function() {
	return getfilesfromfolder("wp-content/uploads/DailyCallReports/", "date");
}
add_shortcode('dailycallreports', 'dailycallreports_function');

function arrestreports_function() {
	return getfilesfromfolder("wp-content/uploads/ArrestReports/", "date");
}
add_shortcode('arrestreports', 'arrestreports_function');

function crashreports_function() {
	return getfilesfromfolder("wp-content/uploads/CrashReports/", "date");
}
add_shortcode('crashreports', 'crashreports_function');
 
function ordinances_function() {
	return getfilesfromfolder("wp-content/uploads/Ordinances/", "alpha");
}
add_shortcode('ordinances', 'ordinances_function');

function agendas_function() { 
	return getfilesfromfolder("wp-content/uploads/CouncilAgendas/", "date");
}
add_shortcode('agendas', 'agendas_function');

function minutes_function() { 
	return getfilesfromfolder("wp-content/uploads/CouncilMinutes/", "date");
}
add_shortcode('minutes', 'minutes_function');

function packets_function() { 
	return getfilesfromfolder("wp-content/uploads/CouncilPackets/", "date");
}
add_shortcode('packets', 'packets_function');

function compPlan_function() { 
	return getfilesfromfolder("wp-content/uploads/2040CompPlan/", "alpha");
}
add_shortcode('compPlan', 'compPlan_function');

// City Forms //

function buildingForms_function() { 
	return getfilesfromfolder("wp-content/uploads/CityForms/Building/", "alpha");
}
add_shortcode('buildingForms', 'buildingForms_function');

function businessForms_function() { 
	return getfilesfromfolder("wp-content/uploads/CityForms/Business/", "alpha");
}
add_shortcode('businessForms', 'businessForms_function');

function generalForms_function() { 
	return getfilesfromfolder("wp-content/uploads/CityForms/GeneralInfo/", "alpha");
}
add_shortcode('generalForms', 'generalForms_function');

function mapsForms_function() { 
	return getfilesfromfolder("wp-content/uploads/CityForms/Maps/", "alpha");
}
add_shortcode('mapsForms', 'mapsForms_function');

function parksForms_function() { 
	return getfilesfromfolder("wp-content/uploads/CityForms/Parks/", "alpha");
}
add_shortcode('parksForms', 'parksForms_function');

function sidewalksForms_function() { 
	return getfilesfromfolder("wp-content/uploads/CityForms/Sidewalks/", "alpha");
}
add_shortcode('sidewalksForms', 'sidewalksForms_function');

function waterForms_function() { 
	return getfilesfromfolder("wp-content/uploads/CityForms/Water/", "alpha");
}
add_shortcode('waterForms', 'waterForms_function');

// Employee Forms //

function employeesForms_function() { 
	return getfilesfromfolder("wp-content/uploads/Employees/Forms/", "alpha");
}
add_shortcode('employeesForms', 'employeesForms_function');

function employeesContactsForms_function() { 
	return getfilesfromfolder("wp-content/uploads/Employees/Contacts/", "alpha");
}
add_shortcode('employeesContactsForms', 'employeesContactsForms_function');

function employeesPoliciesForms_function() { 
	return getfilesfromfolder("wp-content/uploads/Employees/Policies/", "alpha");
}
add_shortcode('employeesPoliciesForms', 'employeesPoliciesForms_function');

// Boards & Commissions Meetings and Agendas

function airportMeetings_function() { 
	return getfilesfromfolder("wp-content/uploads/BoardCommMeetings/AirportCommission", "date");
}
add_shortcode('airportMeetings', 'airportMeetings_function');

function downtownIncentivesMeetings_function() { 
	return getfilesfromfolder("wp-content/uploads/BoardCommMeetings/DowntownIncentives", "date");
}
add_shortcode('downtownIncentivesMeetings', 'downtownIncentivesMeetings_function');

function historicPreservationCommissionMeetings_function() { 
	return getfilesfromfolder("wp-content/uploads/BoardCommMeetings/HistoricPreservationCommission", "date");
}
add_shortcode('historicPreservationCommissionMeetings', 'historicPreservationCommissionMeetings_function');

function parksCommissionMeetings_function() { 
	return getfilesfromfolder("wp-content/uploads/BoardCommMeetings/ParksCommission", "date");
}
add_shortcode('parksCommissionMeetings', 'parksCommissionMeetings_function');

function planningZoningMeetings_function() { 
	return getfilesfromfolder("wp-content/uploads/BoardCommMeetings/PlanningZoning", "date");
}
add_shortcode('planningZoningMeetings', 'planningZoningMeetings_function');

function propertyMaintenanceBoardMeetings_function() { 
	return getfilesfromfolder("wp-content/uploads/BoardCommMeetings/PropertyMaintenanceBoard", "date");
}
add_shortcode('propertyMaintenanceBoardMeetings', 'propertyMaintenanceBoardMeetings_function');

function treeBoardMeetings_function() { 
	return getfilesfromfolder("wp-content/uploads/BoardCommMeetings/TreeBoard", "date");
}
add_shortcode('treeBoardMeetings', 'treeBoardMeetings_function');

function zoningBoardOfAdjustmentsMeetings_function() { 
	return getfilesfromfolder("wp-content/uploads/BoardCommMeetings/ZoningBoardOfAdjustments", "date");
}
add_shortcode('zoningBoardOfAdjustmentsMeetings', 'zoningBoardOfAdjustmentsMeetings_function');
?>